# 1. Geschiedenis
*(4-? slides) Timeline rode draad*

## 1.1 Geen CSS
*2 slides*
  Geen CSS
  Verschillende browsers hadden hun eigen stylesheets, ontwikkelaars waren niet happig om schrijvers en ontwikkelaars van websites die mogelijkheid te geven
  Håkon Wium Lie (bedenker)
  Voorstel
  1994
  
## 1.2 CSS 1.0 
*1 slide*
  1996
  FONT
  COLOR
  TEXT
  BOX-MODEL

## 1.3 CSS 2.0
*1 slide*
  1998 
  POSITION
  TABLES

*1 slide, punten benoemen*
Fragiel en werd niet volledig ondersteund door alle webbrowsers toendertijd
Internet Explorer 3 eerste browser die css implementeerd
NetScape volgde zei het armzalig en alleen maar om een grotere marktaandeel te krijgen (Browserwars, met functionaliteiten gebruikers lokken)

### 1.3.1 CSS 2.1
*1 slide, met foto van tijdlijn css2.1*
  Revised version of css in 2004, 
  browsers pushen om de standaarden te gebruiken
  Retrieved in 2006
  Recommendation in 2011
  
## 1.4 Level 3 and further
*1 - slide, 
  Er is geen echte CSS level 3 specificatie als gehele specificatie.
  Vanwege de constante ontwikkelingen binnen het web, is besloten om de Specificatie te splitsen in modules 
  
  Elke module heeft zijn eigen specificatie, ontwikkeltraject
  
  Ene module verder in ontwikkeling dan andere
  (CSS4) voor x aantal
  
  Op dit moment bestaat de complete CSS specificatie uit de volgende modules
  *GIF laten zien van huidige specificatie*
