# 1. Geschiedenis
*(4-? slides) Timeline rode draad*

## 1.1 Geen CSS
  Geen CSS
  Verschillende browsers hadden hun eigen stylesheets, ontwikkelaars waren niet happig om schrijvers en ontwikkelaars van websites die mogelijkheid te geven
  H�kon Wium Lie (bedenker)
  Voorstel
  1994
  
## 1.2 CSS 1.0
  1996
  
  FONT
  COLOR
  TEXT
  BOX-MODEL

## 1.3 CSS 2.0
  1998 
  POSITION
  TABLES
     
Fragiel en werd niet volledig ondersteund door alle webbrowsers toendertijd
Internet Explorer 3 eerste browser die css implementeerd
NetScape volgde zei het armzalig en alleen maar om een grotere marktaandeel te krijgen (Browserwars, met functionaliteiten gebruikers lokken)

### 1.3.1 CSS 2.1
  Revised version of css in 2004, 
  browsers pushen om de standaarden te gebruiken
  Retrieved in 2006
  Recommendation in 2011
  
## 1.4 Level 3 and further
  Er is geen echte CSS level 3 specificatie als gehele specificatie.
  Vanwege de constante ontwikkelingen binnen het web, is besloten om de Specificatie te splitsen in modules
  
  Elke module heeft zijn eigen specificatie, ontwikkeltraject
  
  Ene module verder in ontwikkeling dan andere
  (CSS4) voor x aantal
  
  Op dit moment bestaat de complete CSS specificatie uit de volgende modules (gif afspelen)
- - -
# 2. Kenmerken van CSS
  Seperation of Content from presentation

  Cascading
    - User Agent Stylesheets (browser)
    - Author StyleSheets
    - User StyleSheets

    - Stappen 1 tm 3 uitleggen

    - Tabel met de order of cascade
    - Voorbeeld geven?

  Specificity
    - Welke selector wordt genomen
    - Tabel laten zien met de specificity waarden

  Inheritance
    - Styling van zijn ouder wordt geovererfd door zijn kinderen
    - Een p element in een div krijgt alle styling eigenschappen die zijn gedeclareerd voor de div element zelfs styling eigenschappen die voor de HTML element zijn gedeclareerd
- - -
# 3. Belangrijkste module/styling dingen
	Boxmodel
		What is the boxmodel
	Float & Clears
		Document Flow
		Position 
			absolute
			relative
			fixed
	Layouts
		Flexbox
		Grid
		Table
		Float & Clears
	Block & Inline Elements
	Values & Units (px, pt, rem, em, vh, fr, etc)
	Media Queries
	Color
	Text & Typography
		Fonts
		Size
		Weight
	Image
	!important
	
	Organize CSS
		OOCSS
		SMACSS
		BEM
- - -
# 4. Hands-on
SASS
- - -
# 5. List of Links (geordend op hoofdstuk)

1. geschiedenis
	1.1 
	https://www.w3.org/People/Bos/stylesheets.html
	http://1997.webhistory.org/www.lists/www-talk.1993q2/0445.html
	http://1997.webhistory.org/www.lists/www-talk.1994q1/0648.html
	https://www.w3.org/People/howcome/p/cascade.html



https://www.w3.org/TR/WD-CSS2-971104/cover.html
https://tools.ietf.org/html/rfc1942
https://tools.ietf.org/html/rfc1866
https://www.w3.org/TR/REC-CSS1-961217
https://www.w3.org/Style/CSS/current-work
https://www.w3.org/TR/css-2017/ latest snapshots
https://tools.ietf.org/html/rfc2070
https://www.w3.org/TR/html401/present/styles.html
https://css-tricks.com
https://www.w3.org/Style/CSS/read
	