# 2. Kenmerken van CSS
*1 slide, punten benoemen*
Seperation of Content from presentation
  - HTML wordt gebruikt om structuur te geven aan webpagina's
  - CSS is voor de presentatie daarvan
  
*1 - slides, *
Cascading 
  - Cascade is een algoritme dat definieert hoe verschillende eigenschappen uit verschillende bronnen worden gecombineerd
  *3 afbeeldingen met verschillende soort stylesheets*
  - User Agent Stylesheets (browser)
  - Author StyleSheets (ontwikkelaar van webpagina's)
  - User StyleSheets (gebruiker van website)
  
  *3 stappen uitleggen met afbeelding*
  - Cascading Order
    1. Alle verschillende stijlen uit verschillende bronnen van één element samenvoegen
    2. Bepalen welke eigenschappen moeten worden toegepast (welke bron is belangrijker)
    - Tabel met de order of cascade
    3. Wat is de specificity van een selector (welke is specifieker)
    
  *extra?*
  - Voorbeeld geven?
  
  
*1 slide, punten benoemen met tabel*
Specificity
  - Welke selector wordt genomen
  - Tabel laten zien met de specificity waarden
  
*1 slide, uitleggen + voorbeeld geven*
Inheritance
  - Styling van zijn ouder wordt geovererfd door zijn kinderen
  - Een p element in een div krijgt alle styling eigenschappen die zijn gedeclareerd voor de div element zelfs styling eigenschappen die voor de HTML element zijn gedeclareerd