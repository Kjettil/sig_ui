# 5. List of Links (geordend op hoofdstuk)

1. geschiedenis
	1.1 
	- https://www.w3.org/People/Bos/stylesheets.html
	- http://1997.webhistory.org/www.lists/www-talk.1993q2/0445.html
	- http://1997.webhistory.org/www.lists/www-talk.1994q1/0648.html
	- https://www.w3.org/People/howcome/p/cascade.html
    
    
2. Kenmerken van CSS
    - https://developer.mozilla.org/en-US/docs/Web/CSS/Cascade
    - https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity
    - https://developer.mozilla.org/en-US/docs/Web/CSS/Inheritance
    
    
3. Modules

3.1 Box Model
    - https://www.w3.org/TR/WD-CSS2-971104/cover.html
    - https://tools.ietf.org/html/rfc1942
    - https://tools.ietf.org/html/rfc1866
    - https://www.w3.org/TR/REC-CSS1-961217
    - https://www.w3.org/Style/CSS/current-work
    - https://www.w3.org/TR/css-2017/ latest snapshots
    - https://tools.ietf.org/html/rfc2070
    - https://www.w3.org/TR/html401/present/styles.html
    - https://css-tricks.com
    - https://www.w3.org/Style/CSS/read